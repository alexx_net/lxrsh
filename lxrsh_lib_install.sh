#!/usr/bin/env sh
# lxrsh_lib_install.sh ver 20250203040506 Copyright alexx, MIT License
# RDFa:deps="[which git curl]"
usage(){ printf "%s [-h]\n\t -h this message\n Installs lxrsh library\n" "$(basename "$0")"; exit 0; }
[ "$1" ]&& echo "$1"|grep -q '\-h' && usage

LXRSH_LIB="https://gitlab.com/alexx_net/lxrsh_lib.git"

# output
err(){ printf "[e] %s\n" "$*" >&2; exit 1;}
log(){ printf "[i] %s\n" "$*" >&1;}

_machine_id(){
    # a locally unique ID for this machine
    if [ "$(which ip)" ]; then
        ip a|grep ether|sort|head -n1|awk '{print $2}'|tr -d ':'
    elif [ "$(which ifconfig)" ]; then
        # ifconfig COULD be depricated now
        ifconfig 2>/dev/null |grep ether|head -n1|awk '{print $NF}'|tr -d ':'
    else
        echo "$HOST"|cut -d- -f2|tr -d ':'
    fi
}

_detect_os(){
    if uname -a|grep -q 'Darwin'; then echo OSX;
    elif uname -a|grep -iq 'linux'; then echo linux;
    else err "Unsupported Operating System - maybe you could add it and make a pull request?"
    fi
}

_find_lib_path(){
    # Ensure that we have everything that we need
    if echo "$OS"|grep -q "OSX"; then
        mkdir -p "$HOME/Library/" 2>/dev/null
        echo "$HOME/Library"
    elif echo "$OS"|grep -q "linux"; then
        mkdir -p "$HOME/.local/lib/" 2>/dev/null
        echo "$HOME/.local/lib"
    else
        err "We haven't dealt with your Operating System yet - maybe you could add it and make a pull request?"
    fi
}

_sanity(){
    # check that git is installed
    [ "$(which git)" ]|| {
        if printf "%s" "$OS"|grep -q "OSX"; then
            log "OSX git installer https://github.com/timcharper/git_osx_installer"
            curl https://sourceforge.net/projects/git-osx-installer/files/latest/download
        elif printf "%s" "$OS"|grep -q "linux"; then
            sudo apt install -y git || \
            sudo yum install -y git || \
            sudo emerge dev-vcs/git || \
            sudo pacman -S git || \
            sudo zypper install git || \
            sudo nix-env -i git || \
            sudo pkg install git || \
            sudo pkgutil -i git ||  \
            sudo pkg install developer/versioning/git || \
            sudo pkg_add git || \
            sudo apk add git || \
            sudo tazpkg get-install git
        else
            err "git required and not located; please install git and try agin"
        fi
    } # / which git
    # check for curl (with wget as a fallback)
}

_create_env(){
    SH_LIB_PATH="$*"
    [ -d "$SH_LIB_PATH" ]|| {
        err "Unable to make $SH_LIB_PATH : $(mkdir -p "$SH_LIB_PATH")"
    }

    [ -f "$SH_LIB_PATH/env" ]&& return

    cat >>"$SH_LIB_PATH/env"<<EOF
#!/bin/sh
# lxrsh_lib/env ver. 20230304T183750Z Copyright alexx, MIT Licence
# RDFa:deps="[]"
case ":\${PATH}:" in
    *:"$SH_LIB_PATH":*)
        ;;
    *)
        export PATH="\$PATH:$SH_LIB_PATH"
        ;;
esac
EOF
}

_uninstall(){
    # Interactive (unless called with -y flag)
    # removal of lxrsh_lib:

    # remove from $SH_ENV
    # remove lxrsh_lib
    # remove $0
    rm "$0"
}

_detect_shell(){
      if echo "${SHELL##*/}"|grep -q "zsh"; then
       printf "%s" "$HOME/.zshenv"
      elif echo "${SHELL##*/}"|grep -q "dash" || echo "${SHELL##*/}"|grep -q "bash"; then
       printf "%s" "$HOME/.bashrc"
      elif echo "${SHELL##*/}"|grep -q "tcsh"; then
       printf "%s" "$HOME/.tcshrc"
      elif echo "${SHELL##*/}"|grep -q "fish"; then
       printf "%s" "$HOME/.config/fish/config.fish"
       #printf "%s" "$HOME/.config/fish/fishd.$(_machine_id)"
      else
       printf "%s" "$HOME/.shrc"
      fi
}

_main(){
    OS="$(_detect_os)"
    SH_LIB_PATH="$(_find_lib_path)"
    [ "$SH_LIB_PATH" ]|| SH_LIB_PATH='.'
    # this is why we use full explicit paths
    mkdir -p "$SH_LIB_PATH" 2>/dev/null
    cd "$SH_LIB_PATH" || exit $?
    git clone $LXRSH_LIB || err "Failed to clone lib"

    SH_ENV="$(_detect_shell)"

    # create env
    _create_env "$SH_LIB_PATH/lxrsh_lib"
    # Install ENV into shell
    printf '# activate lxrsh lib %s\n. %s\n' "$(date +%FT%TZ)" \
        "$SH_LIB_PATH/lxrsh_lib/env" >> "$SH_ENV"
}
_main

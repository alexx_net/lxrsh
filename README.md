# lxrsh

### A shell library

## Install

```sh
curl --proto '=https' --tlsv1.2 -sLSf https://gitlab.com/alexx_net/lxrsh/-/raw/main/lxrsh_lib_install.sh |sh
```
#### What this will do:

1. Download the installer
2. check for sanity, (e.g. that git is installed)
3. clone the library into `$HOME/.local/` [1]
4. Create `$HOME/.local/lxrsh_lib/env` and add it to your personal "Run Control"
    e.g. `$HOME/.bashrc`

[1] or `$HOME/Library` on OSX 

## Authors and acknowledgment
An anonymous collection of hackers that would like the code to speak for itself,
rather than an https://yourlogicalfallacyis.com/appeal-to-authority

## License
`MIT License` for code and
`https://creativecommons.org/licenses/by-sa/4.0/` for text or copy, 
Unless explicitly stated to be under or include some other opensource license.  

## Project status
Beta. Being actively curated and collated.
